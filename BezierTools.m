(* ::Package:: *)

(* ::Title:: *)
(*BezierTools*)


(* ::Text:: *)
(*Rhys Povey <www.rhyspovey.com>*)
(**)
(*14 November 2024*)
(*- Fixed ApproxParallelCubicBezierPoints.*)
(**)
(*1 March 2024*)
(*- Added CurlyBrace.*)
(**)
(*9 March 2023*)
(*- Added CutQuadraticBezierPoints.*)
(**)
(*31 October 2021*)
(*- Added AcuteCircleBezierPoints.*)
(**)
(*4 February 2021*)
(*- Added BezierCurveLine.*)
(**)
(*9 December 2020*)
(*- Added QuarterCircleBezierPoints.*)
(**)
(*12 January 2020*)
(*- Fixed ApproxCutCubicBezierPoints.*)
(*- Improved ApproxParallelCubicBezierPoints.*)
(**)
(*22 August 2019*)
(*- Fixed ExplicitBezierFunction to complete the sum before inserting function argument.*)
(**)
(*15 August 2019*)
(*- First version.*)


(* ::Section:: *)
(*Front End*)


BeginPackage["BezierTools`"];


QuadraticBezierFunction::usage="QuadraticBezierFunction[bezier points][t] gives the parametric function in t\[Element][0,1] of quadratic Bezier curve specified by 3 points.";


CubicBezierFunction::usage="CubicBezierFunction[bezier points][t] gives the parametric function in t\[Element][0,1] of cubic Bezier curve specified by 4 points.";


ExplicitBezierFunction::usage="ExplicitBezierFunction[bezier points][t] gives the parametric function in t\[Element][0,1] of Bezier curve for the specified points.";


(* ::Subsection:: *)
(*Parallel*)


ParallelCubicBezierFunction::usage="ParallelCubicBezierFunction[bezier points, distance][t] gives the parametric function of a curve parallel to specified Bezier curve. Equal to \!\(\*OverscriptBox[\(r\), \(\[RightVector]\)]\)[t] + d \!\(\*SubscriptBox[\(R\), \(\[Pi]/2\)]\)\[CenterDot]Norm[\[DifferentialD]\!\(\*OverscriptBox[\(r\), \(\[RightVector]\)]\)[t]/\[DifferentialD]t].";


ApproxParallelCubicBezierPoints::usage="ApproxParallelCubicBezierPoints[bezier points, distance] gives cubic bezier points for a curve approximately parallel at given distace.";


(* ::Subsection:: *)
(*Cutting*)


CubicBezierLineIntersection::usage="CubicBezierLineIntersection[bezier points, line points] finds the intersection between a cubic bezier curve (specified by four points) and a line (specified by two points).";


CubicBezierLineIntersectionTangent::usage="CubicBezierLineIntersectionTangent[bezier points, line points] finds the tangent to a cubic bezier curve (specified by four points) at the intersection with a line (specified by two points).";


ApproxCutCubicBezierPoints::usage="ApproxCutCubicBezierPoints[bezier points, line points] gives the points specifying a new cubic bezier curve that ends at the intersection with a line.";


CutQuadraticBezierPoints::usage="CutQuadraticBezierPoints[bezier points, line points] gives the points specifying a new quadratic bezier curve that ends at the intersection with a line.";


(* ::Subsection:: *)
(*Other*)


LineIntersection::usage="LineIntersection[line 1 points, line 2 points] gives the intersection of two lines, each specified by two points.";


(* ::Subsection:: *)
(*Circle*)


QuarterCircleBezierPoints::usage="QuarterCircleBezierPoints[center, radius, start angle] gives cubic Bezier points that approximate a quarter circle arc.";


AcuteCircleBezierPoints::usage="AcuteCircleBezierPoints[center, radius, start angle, span angle]  gives cubic Bezier points that approximate an acute circle arc.";


AcuteCircleBezierPoints::span="Span angle `1` must be \[LessEqual] \[Pi]/2";


(* ::Subsection:: *)
(*Line segments*)


BezierCurveLine::usage="BezierCurveLine[Bezier points, number] produces a line of number-1 segments. Takes option VertexColors as a list of length 2 to apply a gradient.";


BezierCurveLine::vertexcolors="Unrecognized VertexColors input.";


(* ::Subsection:: *)
(*Uses*)


CurlyBrace::usage="CurlyBrace[point1, point2, radius] produces a curly brace between two points with specified radius of curvature.";


(* ::Section:: *)
(*Back End*)


Begin["Private`"];


(* ::Text:: *)
(*\[FormalT]\[Element][0,1]*)


QuadraticBezierFunction[\[FormalX]_List/;Length[\[FormalX]]==3][\[FormalT]_]:=(1-\[FormalT])^2 \[FormalX][[1]] + 2(1-\[FormalT]) \[FormalT] \[FormalX][[2]]+\[FormalT]^2 \[FormalX][[3]];


CubicBezierFunction[\[FormalX]_List/;Length[\[FormalX]]==4][\[FormalT]_]:=(1-\[FormalT])^3 \[FormalX][[1]] + 3(1-\[FormalT])^2 \[FormalT] \[FormalX][[2]]+3(1-\[FormalT]) \[FormalT]^2 \[FormalX][[3]]+\[FormalT]^3 \[FormalX][[4]];


ExplicitBezierFunction[\[FormalX]_List]:=\[FormalT]|->Evaluate@With[{\[FormalN]=Length[\[FormalX]]-1,\[FormalP]=\[FormalX]},Sum[Binomial[\[FormalN],\[FormalI]](1-\[FormalT])^(\[FormalN]-\[FormalI]) \[FormalT]^\[FormalI] \[FormalP][[\[FormalI]+1]],{\[FormalI],0,\[FormalN]}]];


(* ::Subsection:: *)
(*Parallel*)


ParallelCubicBezierFunction[\[FormalX]_List/;Length[\[FormalX]]==4,\[FormalD]_][\[FormalT]_]:=(1-\[FormalT])^3 \[FormalX][[1]] + 3(1-\[FormalT])^2 \[FormalT] \[FormalX][[2]]+3(1-\[FormalT]) \[FormalT]^2 \[FormalX][[3]]+\[FormalT]^3 \[FormalX][[4]]+\[FormalD] RotationMatrix[\[Pi]/2] . (-3 (1-\[FormalT])^2 \[FormalX][[1]]+(3(1-\[FormalT])^2-6(1-\[FormalT])\[FormalT])\[FormalX][[2]]+(6(1-\[FormalT])\[FormalT]-3\[FormalT]^2)\[FormalX][[3]]+3 \[FormalT]^2 \[FormalX][[4]])/Sqrt[((-3 (1-\[FormalT])^2 \[FormalX][[1]]+(3(1-\[FormalT])^2-6(1-\[FormalT])\[FormalT])\[FormalX][[2]]+(6(1-\[FormalT])\[FormalT]-3\[FormalT]^2)\[FormalX][[3]]+3 \[FormalT]^2 \[FormalX][[4]])) . ((-3 (1-\[FormalT])^2 \[FormalX][[1]]+(3(1-\[FormalT])^2-6(1-\[FormalT])\[FormalT])\[FormalX][[2]]+(6(1-\[FormalT])\[FormalT]-3\[FormalT]^2)\[FormalX][[3]]+3 \[FormalT]^2 \[FormalX][[4]]))];


ApproxParallelCubicBezierPoints[\[FormalX]_List/;Length[\[FormalX]]==4,\[FormalD]_,OptionsPattern[]]:=Module[{newpts,f1,f2,nmin,fit},
newpts={
\[FormalX][[1]]+\[FormalD] Normalize[RotationMatrix[\[Pi]/2] . (\[FormalX][[2]]-\[FormalX][[1]])],
\[FormalX][[1]]+\[FormalD] Normalize[RotationMatrix[\[Pi]/2] . (\[FormalX][[2]]-\[FormalX][[1]])]+f1 Normalize[\[FormalX][[2]]-\[FormalX][[1]]],
\[FormalX][[4]]+\[FormalD] Normalize[RotationMatrix[\[Pi]/2] . (\[FormalX][[4]]-\[FormalX][[3]])]+f2 Normalize[\[FormalX][[3]]-\[FormalX][[4]]],
\[FormalX][[4]]+\[FormalD] Normalize[RotationMatrix[\[Pi]/2] . (\[FormalX][[4]]-\[FormalX][[3]])]
};

fit=NMinimize[Sum[Norm[ParallelCubicBezierFunction[\[FormalX],\[FormalD]][t]-CubicBezierFunction[newpts][t]],{t,0,1,1/OptionValue[MaxPlotPoints]}],{f1,f2}][[2]];
newpts/.fit
];


Options[ApproxParallelCubicBezierPoints]={MaxPlotPoints->100};


(* ::Subsection:: *)
(*Cutting*)


CubicBezierLineIntersection[bezierpts_List/;Length[bezierpts]==4,linepts_List/;Length[linepts]==2]:=
(linepts[[1]]+\[FormalB](linepts[[2]]-linepts[[1]]))/.First[Select[NSolve[CubicBezierFunction[bezierpts][\[FormalA]]==(linepts[[1]]+\[FormalB](linepts[[2]]-linepts[[1]])),{\[FormalA],\[FormalB]}],(Lookup[#,\[FormalA]]\[Element]Reals)\[And](0<Lookup[#,\[FormalA]]<1)&]];


CubicBezierLineIntersectionTangent[bezierpts_List/;Length[bezierpts]==4,linepts_List/;Length[linepts]==2]:=
Evaluate[D[CubicBezierFunction[bezierpts][\[FormalA]],\[FormalA]]]/.First[Select[NSolve[CubicBezierFunction[bezierpts][\[FormalA]]==(linepts[[1]]+\[FormalB](linepts[[2]]-linepts[[1]])),{\[FormalA],\[FormalB]}],(Lookup[#,\[FormalA]]\[Element]Reals)\[And](0<Lookup[#,\[FormalA]]<1)&]];


ApproxCutCubicBezierPoints[\[FormalX]_List/;Length[\[FormalX]]==4,line_List/;Length[line]==2]:=Module[
{solution,intersection,tangent,distance,f1,f2,nmin,newpts,aint},
solution=First[
Select[
NSolve[CubicBezierFunction[\[FormalX]][\[FormalA]]==(line[[1]]+\[FormalB](line[[2]]-line[[1]])),{\[FormalA],\[FormalB]}],
(Lookup[#,\[FormalA]]\[Element]Reals)\[And](0<Lookup[#,\[FormalA]]<1)&]
];
aint=\[FormalA]/.solution;
intersection=(line[[1]]+\[FormalB](line[[2]]-line[[1]]))/.solution;
tangent=Normalize[Evaluate[D[CubicBezierFunction[\[FormalX]][\[FormalA]],\[FormalA]]]/.solution];

newpts[\[FormalU]_,\[FormalV]_]:={\[FormalX][[1]],\[FormalX][[1]]+\[FormalU](\[FormalX][[2]]-\[FormalX][[1]]),intersection-\[FormalV] tangent,intersection};
nmin=NMinimize[Integrate[(CubicBezierFunction[\[FormalX]][t aint]-CubicBezierFunction[newpts[f1,f2]][t]) . (CubicBezierFunction[\[FormalX]][t aint]-CubicBezierFunction[newpts[f1,f2]][t]),{t,0,1}],{f1,f2}];
newpts[f1,f2]/.nmin[[2]]
];


CutQuadraticBezierPoints[\[FormalX]_List/;Length[\[FormalX]]==3,line_List/;Length[line]==2]:=Module[
{solution,intersection,tangent,tangent1,tangentintersect},
solution=First[
Select[
NSolve[QuadraticBezierFunction[\[FormalX]][\[FormalA]]==(line[[1]]+\[FormalB](line[[2]]-line[[1]])),{\[FormalA],\[FormalB]}],
(Lookup[#,\[FormalA]]\[Element]Reals)\[And](0<Lookup[#,\[FormalA]]<1)&]
];
intersection=(line[[1]]+\[FormalB](line[[2]]-line[[1]]))/.solution;
tangent=Normalize[Evaluate[D[QuadraticBezierFunction[\[FormalX]][\[FormalA]],\[FormalA]]]/.solution];
tangent1=Normalize[Evaluate[D[QuadraticBezierFunction[\[FormalX]][\[FormalA]],\[FormalA]]]/.{\[FormalA]->0}];
tangentintersect=LineIntersection[{\[FormalX][[1]],\[FormalX][[1]]+tangent1},{intersection,intersection+tangent}];

{\[FormalX],tangentintersect,intersection}
];


(* ::Subsection:: *)
(*Other*)


LineIntersection[linepts1_List/;Length[linepts1]==2,linepts2_List/;Length[linepts2]==2]:=
(linepts1[[1]]+\[FormalA](linepts1[[2]]-linepts1[[1]]))/.First[Solve[(linepts1[[1]]+\[FormalA](linepts1[[2]]-linepts1[[1]]))==(linepts2[[1]]+\[FormalB](linepts2[[2]]-linepts2[[1]])),{\[FormalA],\[FormalB]}]];


(* ::Subsection:: *)
(*Circle*)


QuarterCircleBezierPoints[center_List,radius_:1,startangle_:0,direction_:1]:=Module[{c=4/3 (Sqrt[2]-1),d=Sign[direction]},
(radius RotationMatrix[startangle] . #+center)&/@{{1,0},{1,c d},{c,d},{0,d}}
];


AcuteCircleBezierPoints[center_List,radius_:1,startangle_:0,spanangle_:\[Pi]/4]:=Module[{min,c},
min=Minimize[Sign[spanangle] Integrate[(1-Evaluate[(#[[1]]^2+#[[2]]^2)&@CubicBezierFunction[{{1,0},{1,c},{Cos[spanangle],Sin[spanangle]}+c{Sin[spanangle],-Cos[spanangle]},{Cos[spanangle],Sin[spanangle]}}][\[Phi]/spanangle]])^2,{\[Phi],0,spanangle}],c];
(radius RotationMatrix[startangle] . #+center)&/@{{1,0},{1,c},{Cos[spanangle],Sin[spanangle]}+c{Sin[spanangle],-Cos[spanangle]},{Cos[spanangle],Sin[spanangle]}}/.min[[2]]
]/;If[Abs[spanangle]<=\[Pi]/2,True,Message[AcuteCircleBezierPoints::span,spanangle];False];


(* ::Subsection:: *)
(*Line segments*)


BezierCurveLine[\[FormalX]_List,\[FormalN]_Integer,OptionsPattern[]]:=Line[Table[ExplicitBezierFunction[\[FormalX]][\[FormalT]],{\[FormalT],0,1,1/(\[FormalN]-1)}],VertexColors->Which[
OptionValue[VertexColors]===None,None,
Length[OptionValue[VertexColors]]===2,Table[Blend[OptionValue[VertexColors],\[FormalT]],{\[FormalT],0,1,1/(\[FormalN]-1)}],
True,Message[BezierCurveLine::usage];None
]];


Options[BezierCurveLine]={VertexColors->None};


(* ::Subsection:: *)
(*Uses*)


CurlyBrace[p1_,p2_,r_]:=Module[{vec,v1,v2,l,\[Theta],c},
vec=p2-p1;
\[Theta]=ArcTan@@vec;
l=Norm[vec];
v1=vec/l;
v2=RotationMatrix[\[Pi]/2] . v1;

JoinedCurve[{
BezierCurve[QuarterCircleBezierPoints[p1+v1 r,r,\[Theta]+\[Pi],-1]],
Line[{p1+vec/2+v2 r-v1 r/Sqrt[2]}],
BezierCurve[Rest@ApproxCutCubicBezierPoints[QuarterCircleBezierPoints[p1+vec/2+v2 2r-v1 r/Sqrt[2],r,\[Theta]-\[Pi]/2,1],{p1+vec/2+v2 2r-v1 r/Sqrt[2],p1+vec/2+v2 r-v1 (r/Sqrt[2]-r)}]],
BezierCurve[Rest@ApproxCutCubicBezierPoints[QuarterCircleBezierPoints[p2-vec/2+v2 2r+v1 r/Sqrt[2],r,\[Theta]-3\[Pi]/4,1],{p2-vec/2+v2 2r+v1 r/Sqrt[2],p2-vec/2+v2 r+v1 r/Sqrt[2]}]],
Line[{p2-v1 r+v2 r}],
BezierCurve[Rest@QuarterCircleBezierPoints[p2-v1 r,r,\[Theta]+\[Pi]/2,-1]]
}]

];


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
